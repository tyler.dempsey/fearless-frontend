function createCard(name, description, pictureUrl, start, end, location) {
  return `
    <div class="card">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
        <p class="card-text">${description}</p>
        <p class="card-footer">${start} - ${end}</p>
      </div>
    </div>
  `;
}

function alertComponent() {
  return `
  <div id= class="alert alert-primary" role="alert">
  Something's going wrong!
  </div>
  `
}

window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      console.error("ERROR HAPPEN");

        const newHTML = alertComponent();
        const somethingWrong = document.querySelector("#something-wrong");
        somethingWrong.innerHTML = newHTML;
    } else {
      const data = await response.json();
      let i = 1

      for (let conference of data.conferences) {
          if (i > 3) {
              i = 1
          }
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const title = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const start1 = details.conference.starts;
          const start = start1.slice(0,10);
          const end1 = details.conference.ends;
          const end = end1.slice(0,10);
          const location = details.conference.location.name
          const html = createCard(title, description, pictureUrl, start, end, location);
          const column = document.querySelector(`#col${i}`);

          column.innerHTML += html;
          i++
        }
      }

    }
  } catch (e) {
    console.error("ERROR HAPPEN");

        const newHTML = alertComponent();
        const somethingWrong = document.querySelector("#something-wrong");
        somethingWrong.innerHTML = newHTML;
  }

});
